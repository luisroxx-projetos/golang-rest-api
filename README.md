# Golang Rest API

Golang rest API is an API sample for golang using state of art patterns with 90%+ coverage using only integration tests.

# Prerequisites

Golang 1.19

# Run

```
go run source/cmd/app/main.go --migrate
```

Swagger endpoint: localhost:8080/swagger/index.html

# Run Migrations Only

```
go run source/cmd/migrations/main.go -withseed
```

# Linting

```
$ golangci-lint run ./...
```

# Test

## Run tests

```
go test -tags=test -v -coverpkg=./source/... -cover -coverprofile=coverage.out ./source/...
```

## View tests coverage per line

```
go tool cover -html=coverage.out
```

## View total coverage

```
go tool cover -func coverage.out
```

# Build

```
go build -tags=prod source/cmd/app/main.go
```

# Swagger Docs

## Installing go swago tool

```
go install github.com/swaggo/swag/cmd/swag@latest
```

## Generating and updating swagger docs

```
swag init -g source/cmd/app/main.go -o source/swagger --ot go && swag init -g source/cmd/app/main.go -o resources/docs --ot json,yaml
```

## Format swagger

```
swag fmt -g source/cmd/app/main.go
```
