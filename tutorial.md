#Instalation

https://go.dev/dl/
https://github.com/moovweb/gvm

#Configuring IDE

Golang has two majors IDE, VSCode and Goland from Jetbrains, you can use any of those but we recommend a general puorpose one, VSCode

##Installing IDE

##Configuring IDE (https://github.com/golang/vscode-go)

- **Step 1.** If you haven't done so already, install [Go](https://golang.org)
  and the [VS Code Go extension].
  - [Go installation guide]. This extension works best with Go 1.14+.
  - [Managing extensions in VS Code].
- **Step 2.** To activate the extension, open any directory or workspace
  containing Go code. Once activated, the [Go status bar](docs/ui.md) will
  appear in the bottom left corner of the window and show the recognized Go
  version.
- **Step 3.** The extension depends on [a set of extra command-line tools](#tools).
  If they are missing, the extension will show the "⚠️ Analysis Tools Missing"
  warning. Click the notification to complete the installation.

<p align="center">
<img src="docs/images/installtools.gif" width=75%>
<br/>
<em>(Install Missing Tools)</em>
</p>

You are ready to Go :-) &nbsp;&nbsp; 🎉🎉🎉

Please be sure to learn more about the many [features](#features) of this
extension, as well as how to [customize](#customization) them. Take a look at
[Troubleshooting](docs/troubleshooting.md) and [Help](#ask-for-help) for further
guidance.

If you are new to Go, [this article](https://golang.org/doc/code.html) provides
the overview on Go code organization and basic `go` commands. Watch ["Getting
started with VS Code Go"] for an explanation of how to build your first Go
application using VS Code Go.

##Adding Test flag in IDE

To be able to run integration tests using test env configuration, open settings JSON and add the line bellow

`go.testFlags": ["-tags=test"],`

This will ensure the IDE will run the tests with the correct build tag when clicking the test buttons

##Enabling gopls support for BuildTags

For the gopls to be able to recognize files inside build flags, open settings JSON and add the line bellow

```
"gopls": {
	"buildFlags": ["-tags=test,stg,prod"]
},
```

#Learning Golang

Go is a really simple language and, those too tutorials covers all possible operations in the language:

https://gobyexample.com/
https://go.dev/tour/list

For a more detailed, i'd really recommend reading effective go, which covers everything from basic to advanced

https://go.dev/doc/effective_go

#Go Pkg

Pkg.go.dev is a website for discovering and evaluating Go packages and modules.

https://pkg.go.dev/

#List of commands

Go is a tool for managing Go source code, and also offers a list of commands and tools which you can be found in the docs bellow.

https://pkg.go.dev/cmd/go

#Formatter Patterns

Package fmt implements formatted I/O with functions analogous to C's printf and scanf. The format 'verbs' are derived from C's but are simpler.
A list of supported formmaters can be found in the docs bellow.

https://pkg.go.dev/fmt

##FAQS

How do I create contructors?
https://stackoverflow.com/a/18125682

Golang initialization order
https://stackoverflow.com/a/49831018

How do I update struct property inside foreach
https://stackoverflow.com/a/15952415

Parsing dates (Seriously, how date parsers works in golang it's stupid)
https://go.dev/src/time/format.go

Pointer from a function
https://stackoverflow.com/questions/30744965/how-to-get-the-pointer-of-return-value-from-function-call

Different ways of initilizing a value

```
//Using var
var a int //int
var b *string //*string

//Using :=
c := 0 //int
d := "" //string
e := struct{}{} //struct{}
f := &struct{}{} //*struct{}

//Type
g := new(int64) //*int64
h := int64(0) //int64
i := string(1) //string
j := new(struct {
  a string
}) //*struct{a string}

//Map
k := map[string]string{
"a": "b",
}
l := make(map[string]int)

//Array
var m [5]int
n := [5]int{1, 2, 3, 4, 5}
o := [5]int{}

//Slice
var p []string
q := []string{"g", "h", "i"}
r := make([][]int, 3)
s := make([]string, 3)

//Interface
var t interface{}
u := interface{}(nil)
```

Generic Type switch case

```
func convertToType[T any]() T {
	val := interface{}(nil)
	t := new(T)
	interfaceType := interface{}(t)

	// switch v := interface{}(new(T)).(type) {
	switch v := interfaceType.(type) {
	case func():
	case int:
	case float64:
	case time.Duration:
	case []uint8:
	case bool:
	case string:
	default:
		log.Panicf("'%T' type not mapped", v)
	}

	return val.(T)
}
```
