package rabbitmq

import (
	"aplicacao/internal/testcontainers"
	"aplicacao/source/configuration"
	"os"
	"testing"
	"time"

	"gotest.tools/assert"
)

var producer *AmqpSession
var consumer *AmqpSession

func startConsumerListenner() {
	consumer.Consume(func(i string) {
		//EmptyFunction To be able to test speed
	})
}

func sendMessage(message string) {
	producer.Send(message)
}

func TestMain(m *testing.M) {
	testcontainers.SetupRabbitMqTestContainer(
		&testcontainers.RabbitMQConfig{
			DefaultUser:     configuration.AMPQ_USERNAME.Get(),
			DefaultPassword: configuration.AMPQ_PASSWORD.Get(),
		},
	)
	producer = NewQueue(configuration.AMPQ_SOURCE.Get(), configuration.AMPQ_QUEUE_NAME.Get())
	consumer = NewQueue(configuration.AMPQ_SOURCE.Get(), configuration.AMPQ_QUEUE_NAME.Get())
	startConsumerListenner()

	exitVal := m.Run()

	os.Exit(exitVal)
}

func TestMessageShouldTimeout(t *testing.T) {
	sendMessage("rabbimq test message")
	messageConsumed := consumer.WaitForMessage(time.Microsecond * 1)
	assert.Equal(t, false, messageConsumed)
}

func TestRabbitMqShouldExecuteMessage(t *testing.T) {
	sendMessage("abc")
	messageConsumed := consumer.WaitForMessage(time.Second * 1)
	assert.Equal(t, true, messageConsumed)
}

func TestRabbitMqComplexBehavior(t *testing.T) {
	sendMessage("rabbimq test message")
	sendMessage("rabbimq test message")
	messageConsumed := consumer.WaitForMessage(time.Second * 1)
	assert.Equal(t, true, messageConsumed)

	sendMessage("rabbimq test message")
	time.Sleep(1 * time.Second)
	messageConsumed = consumer.WaitForMessage(time.Second * 1)
	assert.Equal(t, false, messageConsumed)

	sendMessage("rabbimq test message")
	messageConsumed = consumer.WaitForMessage(time.Microsecond * 1)
	assert.Equal(t, false, messageConsumed)

	messageConsumed = consumer.WaitForMessage(time.Second * 1)
	assert.Equal(t, false, messageConsumed)
}
