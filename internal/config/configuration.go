package config

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"time"
)

type config[T any] struct {
	value T
}

func MakeConfig[T any](value T) *config[T] {
	return &config[T]{value: value}
}

func (cfg *config[T]) Set(value T) {
	cfg.value = value
}

func (cfg *config[T]) Get() T {
	return cfg.value
}

func (cfg *config[T]) UpdateValueFromEnv(envname string) {
	value, exists := os.LookupEnv(envname)
	if !exists {
		log.Panicf("Environment variable '%s' is not set", envname)
	}

	cfg.value = convertToType[T](value)
}

func convertToType[T any](value string) T {
	var val interface{}

	genericType := fmt.Sprintf("%T", *new(T))
	switch genericType {
	case "int":
		val = valueAsInt(value)
	case "float64":
		val = valueAsFloat(value)
	case "time.Duration":
		val = valueAsDuration(value)
	case "[]uint8":
		val = valueAsByte(value)
	case "bool":
		val = valueAsBool(value)
	case "string":
		val = value
	default:
		log.Panicf("'%s' type not mapped", genericType)
	}

	return val.(T)
}

func valueAsByte(value string) []byte {
	return []byte(value)
}

func valueAsFloat(value string) float64 {
	val, err := strconv.ParseFloat(value, 64)
	if err != nil {
		log.Panicf("'%s' is not a valid float64 type", value)
	}

	return val
}

func valueAsInt(value string) int {
	val, err := strconv.Atoi(value)
	if err != nil {
		log.Panicf("'%s' is not a valid int type", value)
	}

	return val
}

func valueAsBool(value string) bool {
	val, err := strconv.ParseBool(value)
	if err != nil {
		log.Panicf("'%s' is not a valid bool type", value)
	}

	return val
}

func valueAsDuration(value string) time.Duration {
	val, err := time.ParseDuration(value)
	if err != nil {
		log.Panicf("'%s' is not a valid duration type", value)
	}

	return time.Duration(val)
}
