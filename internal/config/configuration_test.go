package config

import (
	"os"
	"testing"
	"time"

	"gotest.tools/assert"
)

var ENV_VARIABLE = "UNIT_TEST_ENV"

func TestShouldConvertToString(t *testing.T) {
	value := "test"
	config := MakeConfig(value)

	assert.Equal(t, config.Get(), value)

	envValue := "updatedValue"
	os.Setenv(ENV_VARIABLE, envValue)
	defer os.Unsetenv(ENV_VARIABLE)

	config.UpdateValueFromEnv(ENV_VARIABLE)
	assert.Equal(t, config.Get(), envValue)
}

func TestShouldConvertToInteger(t *testing.T) {
	value := 1
	config := MakeConfig(value)

	assert.Equal(t, config.Get(), value)

	os.Setenv(ENV_VARIABLE, "100")
	defer os.Unsetenv(ENV_VARIABLE)

	config.UpdateValueFromEnv(ENV_VARIABLE)
	assert.Equal(t, config.Get(), 100)
}

func TestShouldConvertToFloat64(t *testing.T) {
	value := 1.20
	config := MakeConfig(value)

	assert.Equal(t, config.Get(), value)

	os.Setenv(ENV_VARIABLE, "100.10")
	defer os.Unsetenv(ENV_VARIABLE)

	config.UpdateValueFromEnv(ENV_VARIABLE)
	assert.Equal(t, config.Get(), 100.10)
}

func TestShouldConvertAsByte(t *testing.T) {
	value := []byte("Test")
	config := MakeConfig(value)

	assert.Equal(t, string(config.Get()), string(value))

	os.Setenv(ENV_VARIABLE, "Test")
	defer os.Unsetenv(ENV_VARIABLE)

	config.UpdateValueFromEnv(ENV_VARIABLE)
	assert.Equal(t, string(config.Get()), string(value))
}

func TestShouldConvertAsBool(t *testing.T) {
	value := true
	config := MakeConfig(value)

	assert.Equal(t, config.Get(), true)

	os.Setenv(ENV_VARIABLE, "true")
	defer os.Unsetenv(ENV_VARIABLE)

	config.UpdateValueFromEnv(ENV_VARIABLE)
	assert.Equal(t, config.Get(), true)
}

func TestShouldConvertAsDuration(t *testing.T) {
	value := time.Hour
	config := MakeConfig(value)

	assert.Equal(t, config.Get(), time.Hour)

	os.Setenv(ENV_VARIABLE, time.Hour.String())
	defer os.Unsetenv(ENV_VARIABLE)

	config.UpdateValueFromEnv(ENV_VARIABLE)
	assert.Equal(t, config.Get(), time.Hour)
}
