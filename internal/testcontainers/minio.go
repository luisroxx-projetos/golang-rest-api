package testcontainers

import (
	"fmt"

	"github.com/docker/go-connections/nat"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

type MinioContainerConfig struct {
	BaseContainerConfig
	RootUser     string
	RootPassword string
}

const DEFAULT_MINIO_PORT = 9000

func SetupMinioTestContainer(config *MinioContainerConfig) *ContainerResult {
	if config.Port == 0 {
		config.Port = DEFAULT_MINIO_PORT
	}
	port := nat.Port(fmt.Sprintf("%d/tcp", config.Port))

	req := testcontainers.ContainerRequest{
		Image: "quay.io/minio/minio:latest",
		Env: map[string]string{
			"MINIO_ROOT_USER":     config.RootUser,
			"MINIO_ROOT_PASSWORD": config.RootPassword,
		},
		Cmd: []string{"server", "/data"},
		ExposedPorts: []string{
			fmt.Sprintf("%d:%d", config.Port, config.Port),
		},
		WaitingFor: wait.ForLog("Documentation"),
	}
	return setupContainer(req, port, config.PrintContainerLogs)
}
