package testcontainers

import (
	"fmt"

	"github.com/docker/go-connections/nat"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

type RabbitMQConfig struct {
	BaseContainerConfig
	DefaultUser     string
	DefaultPassword string
}

const DEFAULT_RABBITMQ_PORT = 5672

func SetupRabbitMqTestContainer(config *RabbitMQConfig) *ContainerResult {
	if config.Port == 0 {
		config.Port = DEFAULT_RABBITMQ_PORT
	}

	port := nat.Port(fmt.Sprintf("%d/tcp", config.Port))

	req := testcontainers.ContainerRequest{
		Image: "rabbitmq:latest",
		Env: map[string]string{
			"RABBITMQ_DEFAULT_USER": config.DefaultUser,
			"RABBITMQ_DEFAULT_PASS": config.DefaultPassword,
		},
		ExposedPorts: []string{
			fmt.Sprintf("%d:%d", config.Port, config.Port),
		},
		WaitingFor: wait.ForListeningPort(port),
	}

	return setupContainer(req, port, config.PrintContainerLogs)
}
