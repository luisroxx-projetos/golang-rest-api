package testcontainers

import (
	"fmt"

	"github.com/docker/go-connections/nat"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

type MySQLContainerConfig struct {
	BaseContainerConfig
	RootPassword string
	Database     string
}

const DEFAULT_MYSQL_PORT = 3306

func SetupMySQLTestContainer(config *MySQLContainerConfig) *ContainerResult {
	if config.Port == 0 {
		config.Port = DEFAULT_MYSQL_PORT
	}
	port := nat.Port(fmt.Sprintf("%d/tcp", config.Port))

	req := testcontainers.ContainerRequest{
		Image: "mysql:latest",
		Env: map[string]string{
			"MYSQL_ROOT_PASSWORD": config.RootPassword,
			"MYSQL_DATABASE":      config.Database,
		},
		ExposedPorts: []string{
			fmt.Sprintf("%d:%d", config.Port, config.Port),
		},
		WaitingFor: wait.ForListeningPort(port),
	}

	return setupContainer(req, port, config.PrintContainerLogs)
}
