FROM golang:1.19.0-alpine as builder

WORKDIR /go/app

ADD go.mod go.sum ./
RUN go mod download

ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o main source/cmd/app/main.go

# copy artifacts to a clean image
FROM alpine
COPY --from=builder /go/app/main .
ENTRYPOINT [ "./main","--migrate" ]