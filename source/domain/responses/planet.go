package responses

type PlanetResponse struct {
	Id         int    `json:"id" example:"1"`
	Name       string `json:"name" example:"marte"`
	Climate    string `json:"climate" example:"hot"`
	Land       string `json:"land" example:"wet"`
	Atmosphere string `json:"atmosphere" example:"bad"`
}
