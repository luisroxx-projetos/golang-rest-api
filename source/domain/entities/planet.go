package entities

type Planet struct {
	Id         int    `gorm:"column:id;primaryKey;unique"`
	Name       string `gorm:"column:name;unique"`
	Climate    string `gorm:"column:climate"`
	Land       string `gorm:"column:land"`
	Atmosphere string `gorm:"column:atmosphere"`
}

func (b *Planet) TableName() string {
	return "planet"
}
