package entities

func RetrieveAll() []interface{} {
	return []interface{}{
		&Planet{},
		&User{},
	}
}
