package entities

import (
	"aplicacao/source/domain/enumerations"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Id       int                `gorm:"column:id;primaryKey;unique"`
	Name     string             `gorm:"column:name"`
	Username string             `gorm:"column:username;unique"`
	Password string             `gorm:"column:password"`
	Role     enumerations.Roles `gorm:"column:role"`
}

func (u *User) TableName() string {
	return "usuario"
}

func (u *User) BeforeCreate(scope *gorm.Scope) (err error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	scope.SetColumn("password", hash)
	return nil
}
