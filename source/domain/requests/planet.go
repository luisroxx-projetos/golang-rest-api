package requests

type PlanetRequest struct {
	Name       string `json:"name" binding:"required" example:"marte"`
	Climate    string `json:"climate" binding:"required" example:"hot"`
	Land       string `json:"land" binding:"required" example:"wet"`
	Atmosphere string `json:"atmosphere" binding:"required" example:"bad"`
}
