package enumerations

import (
	"database/sql/driver"
)

type Roles int

const (
	NORMAL Roles = iota
	SUPERVISOR
	ADMIN
)

func (e *Roles) Scan(value interface{}) error {
	*e = Roles(value.(int64))
	return nil
}

func (e Roles) Value() (driver.Value, error) {
	return int64(e), nil
}
