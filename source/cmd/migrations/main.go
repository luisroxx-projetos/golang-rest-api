//go:build !test
// +build !test

package main

import (
	"aplicacao/source/migrations"
	"aplicacao/source/repository"
	"flag"
)

func main() {
	withSeed := flag.Bool("withseed", false, "Insert seed data into the database")

	repository.InitDB()
	migrations.AutoMigrate()

	if *withSeed {
		migrations.InsertBaseUsers()
	}
}
