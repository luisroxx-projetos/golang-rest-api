//go:build !test
// +build !test

package main

import (
	"aplicacao/source/io/rabbitmq"
	"aplicacao/source/io/websocket/hubmanager"
	"aplicacao/source/migrations"
	"aplicacao/source/repository"
	"aplicacao/source/routes"
	"flag"
	"log"
)

// @title          Complet go rest
// @version        1.0
// @description    Complete go rest web app
// @termsOfService http://swagger.io/terms/

// @contact.name  API Support
// @contact.url   http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url  http://www.apache.org/licenses/LICENSE-2.0.html

// @host     localhost:8080
// @BasePath /

// @securityDefinitions.apikey ApiKeyAuth
// @in                         header
// @name                       Authorization
func main() {
	hubmanager.InitManager()
	rabbitmq.InitRabbitMQ()
	repository.InitDB()
	route := routes.InitRouter()

	migrate := flag.Bool("migrate", false, "Migrate and synchronize the db before running")
	flag.Parse()

	if *migrate {
		log.Print("Migrating the data...")
		migrations.AutoMigrate()
		migrations.InsertBaseUsers()
		log.Print("Migration finished.")
	}

	route.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
