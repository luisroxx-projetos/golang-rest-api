package routes

import (
	"aplicacao/source/configuration"
	_ "aplicacao/source/swagger"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

func bindSwagger(router *gin.Engine) {
	if configuration.APPLICATION_ENVIRONMENT.Get() != "prod" {
		router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}
}
