package routes

import (
	"aplicacao/source/middleware"

	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	router := gin.New()
	bindSwagger(router)
	bindMiddlewares(router)

	bindActuatorsRoutes(router)
	bindPlanetRoutes(router)
	bindRabbitMQRoutes(router)
	bindGorillaWS(router)

	return router
}

func bindMiddlewares(router *gin.Engine) {
	router.Use(gin.LoggerWithConfig(gin.LoggerConfig{
		SkipPaths: []string{"/metrics"},
	}))
	router.Use(gin.CustomRecovery(middleware.ExceptionMiddleware))
}
