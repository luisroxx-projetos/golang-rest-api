package routes

import (
	"aplicacao/source/io/http/controller_gorillaws"
	"aplicacao/source/middleware"

	"github.com/gin-gonic/gin"
)

func bindGorillaWS(router *gin.Engine) {
	gorillaws := router.Group("/gorillaws")
	gorillaws.Use(middleware.Jwt.MiddlewareFunc())

	gorillaws.GET("", controller_gorillaws.GorillaWebsocket)
}
