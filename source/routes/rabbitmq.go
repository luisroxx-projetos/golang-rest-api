package routes

import (
	"aplicacao/source/io/http/controller_rabbitmq"

	"github.com/gin-gonic/gin"
)

func bindRabbitMQRoutes(router *gin.Engine) {
	rabbitmq := router.Group("/rabbitmq")
	rabbitmq.GET("", controller_rabbitmq.SendMessage)
}
