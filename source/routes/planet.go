package routes

import (
	"aplicacao/source/domain/enumerations"
	"aplicacao/source/io/http/controller_planet"
	"aplicacao/source/middleware"

	"github.com/gin-gonic/gin"
)

func bindPlanetRoutes(router *gin.Engine) {
	planets := router.Group("/planets")
	planets.Use(middleware.Jwt.MiddlewareFunc())

	planets.GET("", controller_planet.ListPlanets)
	planets.GET("/paginated", controller_planet.FindPlanetsPaginated)
	planets.GET("/:id", controller_planet.FindPlanetById)
	planets.POST("", middleware.AuthorizationMiddleware(enumerations.SUPERVISOR), controller_planet.CreatePlanet)
	planets.PUT("/:id", middleware.AuthorizationMiddleware(enumerations.SUPERVISOR), controller_planet.UpdatePlanet)
	planets.DELETE("/:id", middleware.AuthorizationMiddleware(enumerations.ADMIN), controller_planet.DeletePlanet)
}
