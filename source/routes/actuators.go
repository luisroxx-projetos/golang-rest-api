package routes

import (
	"aplicacao/source/io/http/controller_actuator"
	"aplicacao/source/middleware"

	"github.com/gin-gonic/gin"
	"github.com/penglongli/gin-metrics/ginmetrics"
)

func bindActuatorsRoutes(router *gin.Engine) {
	router.GET("/health", controller_actuator.Health)
	router.POST("/login", middleware.Jwt.LoginHandler)
	bindPrometheusMetrics(router)
}

func bindPrometheusMetrics(router *gin.Engine) {
	metrics := ginmetrics.GetMonitor()
	metrics.SetMetricPath("/metrics")
	metrics.Use(router)
}
