package middleware

import (
	"aplicacao/source/domain/exception"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ExceptionMiddleware(c *gin.Context, recovered interface{}) {
	if exception, ok := recovered.(*exception.HttpException); ok {
		c.String(exception.StatusCode, exception.Message)
	} else {
		log.Printf("Exception not mapped: %s", recovered)
		c.AbortWithStatus(http.StatusInternalServerError)
	}
}
