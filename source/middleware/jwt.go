package middleware

import (
	"aplicacao/source/configuration"
	"aplicacao/source/domain/enumerations"
	"aplicacao/source/domain/requests"
	"aplicacao/source/repository"
	"aplicacao/source/utils/controller_utils"
	"log"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type LoginOK struct {
	Code   int    `json:"code" example:"200"`
	Token  string `json:"token" example:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"`
	Expire string `json:"expire" example:"2006-01-02T15:04:05Z07:00"`
}

type LoginError struct {
	StatusCode int    `json:"status_code" example:"401"`
	Message    string `json:"message" example:"Invalid username or password"`
}

type AppClaims struct {
	Name string
	Role enumerations.Roles
}

var Jwt = jwtMiddleware()
var identityKey = configuration.JWT_IDENTITY_KEY.Get()

func jwtMiddleware() *jwt.GinJWTMiddleware {

	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:           configuration.JWT_REALM.Get(),
		Key:             configuration.JWT_SECRET_KEY.Get(),
		Timeout:         configuration.JWT_TIMEOUT.Get(),
		MaxRefresh:      configuration.JWT_MAX_REFRESH.Get(),
		IdentityKey:     identityKey,
		Authenticator:   loginHandler,
		PayloadFunc:     payloadHandler,
		IdentityHandler: identityHandler,
		Authorizator:    autorizatorHandler,
		Unauthorized:    unauthorizedHandler,
		// TokenLookup is a string in the form of "<source>:<name>" that is used
		// to extract token from the request.
		// Optional. Default value "header:Authorization".
		// Possible values:
		// - "header:<name>"
		// - "query:<name>"
		// - "cookie:<name>"
		// - "param:<name>"
		TokenLookup: "header: Authorization, query: token, cookie: jwt",
		// TokenLookup: "query:token",
		// TokenLookup: "cookie:token",

		// TokenHeadName is a string in the header. Default value is "Bearer"
		TokenHeadName: "Bearer",

		// TimeFunc provides the current time. You can override it to use another time value. This is useful for testing or if your server uses a different time zone than your tokens.
		TimeFunc: time.Now,
	})

	if err != nil {
		log.Fatal("JWT Initialization Error: " + err.Error())
	}

	return authMiddleware
}

// Login godoc
// @Summary     Login
// @Description Login and generate jwt auth
// @Tags        Auth
// @Accept      json
// @Produce     json
// @Param       auth body     requests.Auth true "Auth Info"
// @Success     200  {object} middleware.LoginOK
// @Failure     400  {object} exception.HttpException
// @Failure     401  {object} middleware.LoginError
// @Router      /login [post]
func loginHandler(c *gin.Context) (interface{}, error) {
	auth := &requests.Auth{}
	controller_utils.ReadBody(c, auth)

	user, err := repository.FindUserByUsername(auth.Username)

	if err != nil {
		return nil, jwt.ErrFailedAuthentication
	}

	if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(auth.Password)); err != nil {
		return nil, jwt.ErrFailedAuthentication
	}

	return &AppClaims{
		Name: user.Name,
		Role: user.Role,
	}, nil
}

func payloadHandler(data interface{}) jwt.MapClaims {
	user := data.(*AppClaims)

	return jwt.MapClaims{
		identityKey: user.Name,
		"role":      int(user.Role),
	}
}

func identityHandler(c *gin.Context) interface{} {
	return ExtractClaims(c)
}

func autorizatorHandler(data interface{}, c *gin.Context) bool {
	return true
}

func unauthorizedHandler(c *gin.Context, code int, message string) {
	c.JSON(code, LoginError{
		StatusCode: code,
		Message:    message,
	})
}

func ExtractClaims(c *gin.Context) *AppClaims {
	claims := jwt.ExtractClaims(c)
	return &AppClaims{
		Name: claims[identityKey].(string),
		Role: enumerations.Roles(claims["role"].(float64)),
	}
}
