package controller_utils

import (
	"aplicacao/source/domain/exception"
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
)

func ReadBody(c *gin.Context, requestBody interface{}) {
	err := c.ShouldBindJSON(&requestBody)

	if err != nil {
		exception.ThrowBadRequestException(err.Error())
	}
}

func ConvertToInt(stringValue string) int {
	valueConv, err := strconv.Atoi(stringValue)

	if err != nil {
		exception.ThrowBadRequestException(fmt.Sprintf("Error converting parameter to int with error: %s", err))
	}

	return valueConv
}
