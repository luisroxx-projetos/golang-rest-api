package service

import (
	"aplicacao/source/domain/entities"
	"aplicacao/source/domain/exception"
	"aplicacao/source/domain/requests"
	"aplicacao/source/domain/responses"
	"aplicacao/source/repository"
	"errors"
	"fmt"

	"github.com/jinzhu/gorm"
)

func ListPlanets() []responses.PlanetResponse {
	planets, err := repository.ListPlanets()
	if err != nil {
		exception.ThrowInternalServerException(
			fmt.Sprintf("Error while trying to find planets with error: %s", err),
		)
	}

	planetsResponse := []responses.PlanetResponse{}
	for _, planet := range planets {
		planetsResponse = append(planetsResponse, *mapToPlanetResponse(&planet))
	}

	return planetsResponse
}

func FindPlanetsPaginated(limit int, page int) *repository.Pagination {
	paginated, err := repository.FindPlanetsPaginated(limit, page)
	if err != nil {
		exception.ThrowInternalServerException(
			fmt.Sprintf("Error while trying to find planets paginated with error: %s", err),
		)
	}

	planetsResponse := []responses.PlanetResponse{}
	for _, planet := range paginated.Rows.([]entities.Planet) {
		planetsResponse = append(planetsResponse, *mapToPlanetResponse(&planet))
	}
	paginated.Rows = planetsResponse

	return paginated
}

func FindPlanetById(id int) *responses.PlanetResponse {
	planet, err := repository.FindPlanetById(id)
	if err != nil {
		exception.ThrowNotFoundException(fmt.Sprintf("Planet with %d was not found", id))
	}

	return mapToPlanetResponse(planet)
}

func InsertPlanet(request *requests.PlanetRequest) {
	repository.UsingTransactional(func(tx *repository.TransactionalOperation) error {
		exists := repository.ExistsPlanetByName(request.Name)

		if exists {
			return exception.BadRequestException(
				fmt.Sprintf("Planet with name %s already exists", request.Name),
			)
		}

		planet := &entities.Planet{
			Name:       request.Name,
			Climate:    request.Climate,
			Land:       request.Land,
			Atmosphere: request.Atmosphere,
		}

		if err := repository.CreatePlanet(planet, tx); err != nil {
			return exception.InternalServerException(
				fmt.Sprintf("Error while trying to insert new planet with error: %s", err),
			)
		}

		return nil
	})
}

func UpdatePlanet(request *requests.PlanetRequest, id int) {
	repository.UsingTransactional(func(tx *repository.TransactionalOperation) error {
		planet, err := repository.FindPlanetById(id)
		if err != nil {
			return exception.NotFoundException(
				fmt.Sprintf("Planet with id {%d} not found", id))
		}

		planet.Name = request.Name
		planet.Climate = request.Climate
		planet.Land = request.Land
		planet.Atmosphere = request.Atmosphere

		if err := repository.UpdatePlanet(planet, tx); err != nil {
			return exception.InternalServerException(
				fmt.Sprintf("Error ocurred while trying to update new planet with error: %s", err))
		}

		return nil
	})
}

func DeletePlanet(id int) {
	repository.UsingTransactional(func(tx *repository.TransactionalOperation) error {
		if err := repository.DeletePlanet(id, tx); err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return exception.NotFoundException(
					fmt.Sprintf("Planet with id {%d} not found", id))
			}

			return exception.InternalServerException(
				fmt.Sprintf("Error ocurred while trying to delete new planet with error: %s", err))
		}

		return nil
	})
}

func mapToPlanetResponse(planet *entities.Planet) (response *responses.PlanetResponse) {
	return &responses.PlanetResponse{
		Id:         planet.Id,
		Name:       planet.Name,
		Climate:    planet.Climate,
		Land:       planet.Land,
		Atmosphere: planet.Atmosphere,
	}
}
