package controller_test

import (
	"aplicacao/internal/testcontainers"
	_ "aplicacao/source/_testinit"
	"aplicacao/source/configuration"
	"aplicacao/source/domain/enumerations"
	"aplicacao/source/io/rabbitmq"
	"aplicacao/source/io/websocket/hubmanager"
	"aplicacao/source/migrations"
	"aplicacao/source/repository"
	"aplicacao/source/routes"
	"aplicacao/source/test/testutils"
	"log"
	"os"
	"sync"
	"testing"
	"time"

	"github.com/appleboy/gofight/v2"
	"github.com/gin-gonic/gin"
)

/*
	This file needs to be the first one on the order inside the package
	otherwise tests can fail because init functions ordering
*/

var RunTest = testutils.CreateForEach(beforeEach, AfterEach)
var runningContainers = []*testcontainers.ContainerResult{}
var router = &gin.Engine{}

var (
	adminToken      = testutils.GenerateJwtToken("ADMIN", enumerations.ADMIN)
	supervisorToken = testutils.GenerateJwtToken("SUPERVISOR", enumerations.SUPERVISOR)
	normalToken     = testutils.GenerateJwtToken("NORMAL", enumerations.NORMAL)
)

func TestMain(m *testing.M) {
	log.Println("Starting test setup")
	start := time.Now()
	beforeAll()
	log.Printf("Setup took %s seconds\n", time.Since(start))
	exitVal := m.Run()

	for _, container := range runningContainers {
		container.Terminate()
	}

	os.Exit(exitVal)
}

func beforeAll() {
	wg := &sync.WaitGroup{}
	testutils.RunInParallel(wg, startMySqlContainer)
	testutils.RunInParallel(wg, startRabbitMqContainer)
	wg.Wait()

	hubmanager.InitManager()
	rabbitmq.InitRabbitMQ()
	repository.InitDB()
	router = routes.InitRouter()
}

func startMySqlContainer() {
	mysqlContainer := testcontainers.SetupMySQLTestContainer(
		&testcontainers.MySQLContainerConfig{
			Database:     configuration.DATABASE_NAME.Get(),
			RootPassword: configuration.DATABASE_PASSWORD.Get(),
		},
	)
	runningContainers = append(runningContainers, mysqlContainer)
}

func startRabbitMqContainer() {
	rabbitMqContainer := testcontainers.SetupRabbitMqTestContainer(
		&testcontainers.RabbitMQConfig{
			DefaultUser:     configuration.AMPQ_USERNAME.Get(),
			DefaultPassword: configuration.AMPQ_PASSWORD.Get(),
		},
	)
	runningContainers = append(runningContainers, rabbitMqContainer)
}

func beforeEach() {
	migrations.AutoMigrate()
}

func AfterEach() {
	migrations.DropAll()
	hubmanager.ClearHubs()
}

func baseRest(role enumerations.Roles) *gofight.RequestConfig {
	jwt := jwtFromRole(role)

	return &gofight.RequestConfig{
		Debug: true,
		Headers: gofight.H{
			"Authorization": "Bearer " + jwt,
		}}
}

func emptyRest() *gofight.RequestConfig {
	return &gofight.RequestConfig{Debug: true}
}

func jwtFromRole(role enumerations.Roles) string {
	switch role {
	case enumerations.ADMIN:
		return adminToken
	case enumerations.SUPERVISOR:
		return supervisorToken
	default:
		return normalToken
	}
}
