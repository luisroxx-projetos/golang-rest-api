package controller_test

import (
	"aplicacao/source/domain/entities"
	"aplicacao/source/domain/enumerations"
	"aplicacao/source/domain/requests"
	"aplicacao/source/repository"
	"net/http"
	"testing"

	"github.com/appleboy/gofight/v2"
	"github.com/stretchr/testify/assert"
)

func TestInvalidRequestBody(t *testing.T) {
	RunTest(func() {
		repository.CreatePlanet(&entities.Planet{
			Id:         1,
			Name:       "Name",
			Climate:    "Climate",
			Land:       "Land",
			Atmosphere: "Atmosphere",
		})

		requestBody := requests.PlanetRequest{Climate: "tempered", Land: "florests and mountains", Atmosphere: "Type III"}

		baseRest(enumerations.SUPERVISOR).
			PUT("/planets/1").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
				assert.Equal(t, `Key: 'PlanetRequest.Name' Error:Field validation for 'Name' failed on the 'required' tag`, response.Body.String())
			})
	})
}

func TestInvalidPathParameterNotInt(t *testing.T) {
	RunTest(func() {
		repository.CreatePlanet(&entities.Planet{
			Id:         1,
			Name:       "Name",
			Climate:    "Climate",
			Land:       "Land",
			Atmosphere: "Atmosphere",
		})

		requestBody := requests.PlanetRequest{Name: "marte", Climate: "tempered", Land: "florests and mountains", Atmosphere: "Type III"}

		baseRest(enumerations.SUPERVISOR).
			PUT("/planets/1.1").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
				assert.Equal(t, `Error converting parameter to int with error: strconv.Atoi: parsing "1.1": invalid syntax`, response.Body.String())
			})
	})
}
