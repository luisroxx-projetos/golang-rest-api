package controller_test

import (
	"aplicacao/source/domain/entities"
	"aplicacao/source/domain/enumerations"
	"aplicacao/source/domain/requests"
	"aplicacao/source/repository"
	"net/http"
	"testing"

	"github.com/appleboy/gofight/v2"
	"github.com/stretchr/testify/assert"
)

func TestLoginOK(t *testing.T) {
	RunTest(func() {
		repository.CreateUser(&entities.User{
			Name:     "test",
			Username: "admin",
			Password: "admin",
			Role:     enumerations.ADMIN,
		})

		requestBody := requests.Auth{
			Username: "admin",
			Password: "admin",
		}

		emptyRest().POST("/login").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.Contains(t, response.Body.String(), `"code":200,"expire"`)
			})
	})
}

func TestLoginInvalidFields(t *testing.T) {
	RunTest(func() {
		requestBody := requests.Auth{
			Username: "admin",
		}

		emptyRest().POST("/login").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
				assert.Equal(t, `Key: 'Auth.Password' Error:Field validation for 'Password' failed on the 'required' tag`, response.Body.String())
			})
	})
}

func TestLoginUsernameNotFound(t *testing.T) {
	RunTest(func() {
		requestBody := requests.Auth{
			Username: "admin",
			Password: "admin",
		}

		emptyRest().POST("/login").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusUnauthorized, response.Code)
			})
	})
}

func TestLoginInvalidPassword(t *testing.T) {
	RunTest(func() {
		repository.CreateUser(&entities.User{
			Name:     "test",
			Username: "admin",
			Password: "admin",
			Role:     enumerations.ADMIN,
		})

		requestBody := requests.Auth{
			Username: "admin",
			Password: "wrongpassword",
		}

		emptyRest().POST("/login").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusUnauthorized, response.Code)
				assert.Equal(t, `{"status_code":401,"message":"incorrect Username or Password"}`, response.Body.String())
			})
	})
}
