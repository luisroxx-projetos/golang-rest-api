package controller_test

import (
	"net/http"
	"testing"

	"github.com/appleboy/gofight/v2"
	"github.com/stretchr/testify/assert"
)

func TestHealth(t *testing.T) {
	RunTest(func() {
		emptyRest().GET("/health").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(t, `{"status":"OK"}`, response.Body.String())
			})
	})
}
