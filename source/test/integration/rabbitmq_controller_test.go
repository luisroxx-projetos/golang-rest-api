package controller_test

import (
	"aplicacao/source/domain/enumerations"
	"aplicacao/source/io/rabbitmq"
	"net/http"
	"testing"
	"time"

	"github.com/appleboy/gofight/v2"
	"github.com/stretchr/testify/assert"
)

func TestRabbitMqSendMessage(t *testing.T) {
	RunTest(func() {
		baseRest(enumerations.NORMAL).
			GET("/rabbitmq").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
			})
	})
}

func TestRabbitMqReceiveMessage(t *testing.T) {
	RunTest(func() {
		rabbitmq.SendMessage("abc")
		messageConsumed := rabbitmq.Consumer.WaitForMessage(time.Second * 5)
		assert.Equal(t, true, messageConsumed)
	})
}
