package controller_test

import (
	"aplicacao/source/domain/entities"
	"aplicacao/source/domain/enumerations"
	"aplicacao/source/domain/requests"
	"aplicacao/source/repository"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/appleboy/gofight/v2"
	"github.com/stretchr/testify/assert"
)

func TestFindPlanetById(t *testing.T) {
	RunTest(func() {
		repository.CreatePlanet(&entities.Planet{
			Id:         1,
			Name:       "MockNameGet",
			Climate:    "MockClimateGet",
			Land:       "MockLandGet",
			Atmosphere: "MockAtmosphereGet",
		})

		baseRest(enumerations.NORMAL).
			GET("/planets/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(
					t,
					`{"id":1,"name":"MockNameGet","climate":"MockClimateGet","land":"MockLandGet","atmosphere":"MockAtmosphereGet"}`,
					response.Body.String(),
				)
			})
	})
}

func TestFindPlanetByIdNotFound(t *testing.T) {
	RunTest(func() {
		baseRest(enumerations.NORMAL).
			GET("/planets/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestFindPaginatedDefaultValues(t *testing.T) {
	RunTest(func() {
		baseRest(enumerations.NORMAL).
			GET("/planets/paginated").
			SetQuery(gofight.H{
				"page":  "0",
				"limit": "0",
			}).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				paginatorResponse := &repository.Pagination{}
				json.Unmarshal(response.Body.Bytes(), paginatorResponse)

				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(t, 10, paginatorResponse.Limit)
				assert.Equal(t, 1, paginatorResponse.Page)
				assert.Equal(t, 0, paginatorResponse.TotalPages)
				assert.Equal(t, 0, len(paginatorResponse.Rows.([]interface{})))
			})
	})
}

func TestFindPaginated(t *testing.T) {
	RunTest(func() {
		for i := 0; i < 30; i++ {
			repository.CreatePlanet(&entities.Planet{
				Name:       fmt.Sprintf("MockName%d", i),
				Climate:    fmt.Sprintf("MockClimate%d", i),
				Land:       fmt.Sprintf("MockLand%d", i),
				Atmosphere: fmt.Sprintf("MockAtmosphere%d", i),
			})
		}

		baseRest(enumerations.NORMAL).
			GET("/planets/paginated").
			SetQuery(gofight.H{
				"page":  "1",
				"limit": "10",
			}).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				paginatorResponse := &repository.Pagination{}
				json.Unmarshal(response.Body.Bytes(), paginatorResponse)

				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(t, 10, paginatorResponse.Limit)
				assert.Equal(t, 1, paginatorResponse.Page)
				assert.Equal(t, 3, paginatorResponse.TotalPages)
				assert.Equal(t, 10, len(paginatorResponse.Rows.([]interface{})))
			})
	})
}

func TestFindAllPlanets(t *testing.T) {
	RunTest(func() {
		repository.CreatePlanet(&entities.Planet{
			Name:       "MockName",
			Climate:    "MockClimate",
			Land:       "MockLand",
			Atmosphere: "MockAtmosphere",
		})

		repository.CreatePlanet(&entities.Planet{
			Name:       "MockName2",
			Climate:    "MockClimate2",
			Land:       "MockLand2",
			Atmosphere: "MockAtmosphere2",
		})

		baseRest(enumerations.NORMAL).
			GET("/planets").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(
					t,
					`[{"id":1,"name":"MockName","climate":"MockClimate","land":"MockLand","atmosphere":"MockAtmosphere"},{"id":2,"name":"MockName2","climate":"MockClimate2","land":"MockLand2","atmosphere":"MockAtmosphere2"}]`,
					response.Body.String(),
				)
			})
	})
}

func TestFindAllPlanetsEmpty(t *testing.T) {
	RunTest(func() {
		baseRest(enumerations.NORMAL).
			GET("/planets").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.Equal(
					t,
					`[]`,
					response.Body.String(),
				)
			})
	})
}

func TestPostPlanet(t *testing.T) {
	RunTest(func() {
		requestBody := requests.PlanetRequest{Name: "MartePost", Climate: "tempered", Land: "florests and mountains", Atmosphere: "Type III"}

		baseRest(enumerations.SUPERVISOR).
			POST("/planets").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				assert.True(t, repository.ExistsPlanetByName("MartePost"))
			})
	})
}

func TestPostPlanetWithExistingName(t *testing.T) {
	RunTest(func() {
		repository.CreatePlanet(&entities.Planet{
			Name:       "Name",
			Climate:    "Climate",
			Land:       "Land",
			Atmosphere: "Atmosphere",
		})

		requestBody := requests.PlanetRequest{Name: "Name", Climate: "tempered", Land: "florests and mountains", Atmosphere: "Type III"}

		baseRest(enumerations.SUPERVISOR).
			POST("/planets").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
				assert.Equal(t, `Planet with name Name already exists`, response.Body.String())
			})
	})
}

func TestPostPlanetForbidden(t *testing.T) {
	RunTest(func() {
		requestBody := requests.PlanetRequest{Name: "MartePost", Climate: "tempered", Land: "florests and mountains", Atmosphere: "Type III"}

		baseRest(enumerations.NORMAL).
			POST("/planets").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusForbidden, response.Code)
			})
	})
}

func TestUpdatePlanet(t *testing.T) {
	RunTest(func() {
		repository.CreatePlanet(&entities.Planet{
			Id:         1,
			Name:       "Name",
			Climate:    "Climate",
			Land:       "Land",
			Atmosphere: "Atmosphere",
		})

		requestBody := requests.PlanetRequest{Name: "marte", Climate: "tempered", Land: "florests and mountains", Atmosphere: "Type III"}

		baseRest(enumerations.SUPERVISOR).
			PUT("/planets/1").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				planetAfterUpdate, _ := repository.FindPlanetById(1)

				assert.Equal(t, "marte", planetAfterUpdate.Name)
				assert.Equal(t, "tempered", planetAfterUpdate.Climate)
				assert.Equal(t, "florests and mountains", planetAfterUpdate.Land)
				assert.Equal(t, "Type III", planetAfterUpdate.Atmosphere)
			})
	})
}

func TestUpdatePlanetForbidden(t *testing.T) {
	RunTest(func() {
		requestBody := requests.PlanetRequest{Name: "marte", Climate: "tempered", Land: "florests and mountains", Atmosphere: "Type III"}

		baseRest(enumerations.NORMAL).
			PUT("/planets/1").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusForbidden, response.Code)
			})
	})
}

func TestUpdatePlanetNotFound(t *testing.T) {
	RunTest(func() {
		requestBody := requests.PlanetRequest{Name: "marte", Climate: "tempered", Land: "florests and mountains", Atmosphere: "Type III"}

		baseRest(enumerations.SUPERVISOR).
			PUT("/planets/1").
			SetJSONInterface(requestBody).
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}

func TestDeletePlanet(t *testing.T) {
	RunTest(func() {
		repository.CreatePlanet(&entities.Planet{
			Id:         1,
			Name:       "MockNameDelete",
			Climate:    "MockClimateDelete",
			Land:       "MockLandDelete",
			Atmosphere: "MockAtmosphereDelete",
		})

		baseRest(enumerations.ADMIN).
			DELETE("/planets/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusOK, response.Code)
				planet, err := repository.FindPlanetById(1)
				assert.Empty(t, planet)
				assert.Error(t, err)
			})
	})
}

func TestDeletePlanetForbidden(t *testing.T) {
	RunTest(func() {
		baseRest(enumerations.SUPERVISOR).
			DELETE("/planets/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusForbidden, response.Code)
			})
	})
}

func TestDeletePlanetNotFound(t *testing.T) {
	RunTest(func() {
		baseRest(enumerations.ADMIN).
			DELETE("/planets/1").
			Run(router, func(response gofight.HTTPResponse, request gofight.HTTPRequest) {
				assert.Equal(t, http.StatusNotFound, response.Code)
			})
	})
}
