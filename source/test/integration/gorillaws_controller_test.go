package controller_test

import (
	"aplicacao/source/io/websocket/hubmanager"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/websocket"
	"gotest.tools/assert"
)

// func (rc *RequestConfig) Run(r http.Handler, response ResponseFunc) {
// 	req, w := rc.initTest()
// 	r.ServeHTTP(w, req)
// 	response(w, req)
// }

// func Run(router http.Handler, response ResponseFunc) {
// 	server := httptest.NewServer(router)
// 	ws, _, err := websocket.DefaultDialer.Dial(wsURL, http.Header{
// 		"Authorization": []string{"Bearer " + adminToken},
// 	})
// 	response(w, req)
// }

// https://quii.gitbook.io/learn-go-with-tests/build-an-application/websockets
func TestGorillaWS(t *testing.T) {
	RunTest(func() {
		hubname := "testhub"

		server := httptest.NewServer(router)
		defer server.Close()

		hubmanager.CreateHub(hubname)

		wsURL := fmt.Sprintf("ws%s/gorillaws?roomname=%s", strings.TrimPrefix(server.URL, "http"), hubname)
		ws := getWsConn(t, wsURL)
		defer ws.Close()

		if err := ws.WriteMessage(websocket.TextMessage, []byte("message 1")); err != nil {
			t.Fatalf("could not send message over ws connection %v", err)
		}

		_, messageBytes, _ := ws.ReadMessage()
		assert.Equal(t, string(messageBytes), "message 1")

		createdHub, _ := hubmanager.RetrieveHub(hubname)
		assert.Equal(t, createdHub.ConnectedClients(), 1)

		hubmanager.RemoveHub(hubname)
		assert.Equal(t, createdHub.ConnectedClients(), 0)

		_, _, err := ws.ReadMessage()
		assert.Error(t, err, "websocket: close 1000 (normal)")
	})
}

func TestGorillaWSHubNotFound(t *testing.T) {
	RunTest(func() {
		hubname := "testhub"

		server := httptest.NewServer(router)
		defer server.Close()

		wsURL := fmt.Sprintf("ws%s/gorillaws?roomname=%s", strings.TrimPrefix(server.URL, "http"), hubname)
		_, _, err := websocket.DefaultDialer.Dial(wsURL, nil)

		if err != nil {
			assert.Error(t, err, "websocket: bad handshake")
		}
	})
}

func TestGorillaWsLoginError(t *testing.T) {
	RunTest(func() {
		server := httptest.NewServer(router)
		defer server.Close()

		wsURL := fmt.Sprintf("ws%s/gorillaws", strings.TrimPrefix(server.URL, "http"))
		_, _, err := websocket.DefaultDialer.Dial(wsURL, nil)

		if err != nil {
			assert.Error(t, err, "websocket: bad handshake")
		}
	})
}

func getWsConn(t *testing.T, url string) *websocket.Conn {
	ws, _, err := websocket.DefaultDialer.Dial(url, http.Header{
		"Authorization": []string{"Bearer " + adminToken},
	})

	if err != nil {
		t.Fatalf("could not open a ws connection on %s %v", url, err)
	}

	return ws
}
