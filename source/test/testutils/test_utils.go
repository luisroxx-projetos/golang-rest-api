package testutils

import (
	"aplicacao/source/domain/enumerations"
	"aplicacao/source/middleware"
	"sync"
)

func CreateForEach(setUp func(), tearDown func()) func(func()) {
	return func(testFunc func()) {
		setUp()
		testFunc()
		tearDown()
	}
}

func GenerateJwtToken(name string, role enumerations.Roles) string {
	token, _, _ := middleware.Jwt.TokenGenerator(&middleware.AppClaims{
		Name: name,
		Role: role,
	})

	return token
}

func RunInParallel(wg *sync.WaitGroup, work func()) {
	wg.Add(1)
	go func() {
		defer wg.Done()
		work()
	}()
}
