package gorillaws

import (
	"aplicacao/source/io/websocket/hubmanager"
	"net/http"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func GorillaHandler(hub *hubmanager.Hub, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return
	}

	client := hubmanager.NewClient(hub, conn)
	client.StartListenning()
}
