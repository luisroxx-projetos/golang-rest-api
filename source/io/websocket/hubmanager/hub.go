package hubmanager

import (
	"time"

	"github.com/gorilla/websocket"
)

type Hub struct {
	name       string
	clients    map[string]*Client
	broadcast  chan []byte
	register   chan *Client
	unregister chan *Client
	kill       chan interface{}
}

func (h *Hub) ConnectedClients() int {
	return len(h.clients)
}

func (h *Hub) DisconnectClient(id string) {
	if client, ok := h.clients[id]; ok {
		delete(h.clients, id)
		closeNormalClosure := websocket.FormatCloseMessage(websocket.CloseNormalClosure, "")
		client.conn.WriteControl(websocket.CloseMessage, closeNormalClosure, time.Now().Add(time.Second))
		close(client.send)
	}
}

func (h *Hub) DisconnectAllClients() {
	for uuid, _ := range h.clients {
		h.DisconnectClient(uuid)
	}
}

func (h *Hub) listen(emptyHubTimeout time.Duration) {
	isAlive := true
	for isAlive {
		if len(h.clients) == 0 {
			timer := time.NewTimer(emptyHubTimeout)

			select {
			case client := <-h.register:
				h.clients[client.id] = client
			case <-timer.C:
				isAlive = false
			case <-h.kill:
				isAlive = false
			}

			timer.Stop()

			continue
		}

		select {
		case client := <-h.register:
			h.clients[client.id] = client
		case client := <-h.unregister:
			if _, ok := h.clients[client.id]; ok {
				delete(h.clients, client.id)
				close(client.send)
			}
		case message := <-h.broadcast:
			for _, client := range h.clients {
				select {
				case client.send <- message:
				default:
					// ??
					// delete(h.clients, uuid)
					// close(client.send)
				}
			}
		case <-h.kill:
			isAlive = false
		}
	}
}
