package hubmanager

import (
	"aplicacao/source/configuration"
	"errors"
)

var hubs map[string]*Hub

func InitManager() {
	hubs = make(map[string]*Hub)
}

func RetrieveHub(name string) (*Hub, error) {
	hub, exists := hubs[name]
	if !exists {
		return nil, errors.New("Hub not found")
	}
	return hub, nil
}

func CreateHub(name string) error {
	_, exists := hubs[name]
	if exists {
		return errors.New("Hub with name %s already exists")
	}

	hub := &Hub{
		name:       name,
		broadcast:  make(chan []byte),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[string]*Client),
		kill:       make(chan interface{}),
	}

	hubs[name] = hub
	go executeHub(hub)
	return nil
}

func RemoveHub(name string) error {
	hub, exists := hubs[name]
	if !exists {
		return errors.New("Hub not found")
	}

	hub.kill <- struct{}{}
	<-hub.kill
	return nil
}

func ClearHubs() {
	for _, hub := range hubs {
		hub.kill <- struct{}{}
		<-hub.kill
	}
}

func executeHub(hub *Hub) {
	hub.listen(configuration.HUB_IDLE_TIMEOUT.Get())
	hub.DisconnectAllClients()
	delete(hubs, hub.name)
	select {
	case hub.kill <- struct{}{}:
	default:
	}
}
