package controller_planet

import (
	"aplicacao/source/service"
	"aplicacao/source/utils/controller_utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

// FindPlanets godoc
// @Summary     Find Planets
// @Description Find all planets
// @Tags        Planet
// @Accept      json
// @Produce     json
// @Param       limit query    int                                                    true "Quantity of itens per page"
// @Param       page  query    int                                                    true "Number of the page"
// @Success     200   {object} repository.Pagination{Rows=[]responses.PlanetResponse} "asc"
// @Failure     401   {object} middleware.LoginError
// @Security    ApiKeyAuth
// @Router      /planets/paginated [get]
func FindPlanetsPaginated(c *gin.Context) {
	limit := controller_utils.ConvertToInt(c.DefaultQuery("limit", "10"))
	page := controller_utils.ConvertToInt(c.DefaultQuery("page", "1"))

	planets := service.FindPlanetsPaginated(limit, page)
	c.JSON(http.StatusOK, planets)
}
