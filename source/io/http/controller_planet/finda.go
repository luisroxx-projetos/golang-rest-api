package controller_planet

import (
	"aplicacao/source/service"
	"aplicacao/source/utils/controller_utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

// FindPlanetById godoc
// @Summary     Find Planet by Id
// @Description Find a planet using the desired id
// @Tags        Planet
// @Accept      json
// @Produce     json
// @Param       id  path     int true "Planet ID"
// @Success     200 {object} responses.PlanetResponse
// @Failure     400 {object} exception.HttpException
// @Failure     401 {object} middleware.LoginError
// @Failure     404 {object} exception.HttpException
// @Security    ApiKeyAuth
// @Router      /planets/{id} [get]
func FindPlanetById(c *gin.Context) {
	id := controller_utils.ConvertToInt(c.Params.ByName("id"))
	planet := service.FindPlanetById(id)
	c.JSON(http.StatusOK, planet)
}
