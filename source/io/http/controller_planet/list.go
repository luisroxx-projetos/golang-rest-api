package controller_planet

import (
	"aplicacao/source/service"
	"net/http"

	"github.com/gin-gonic/gin"
)

// ListPlanets godoc
// @Summary     List Planets
// @Description List all planets
// @Tags        Planet
// @Accept      json
// @Produce     json
// @Success     200 {array}  responses.PlanetResponse
// @Failure     401 {object} middleware.LoginError
// @Security    ApiKeyAuth
// @Router      /planets [get]
func ListPlanets(c *gin.Context) {
	planets := service.ListPlanets()
	c.JSON(http.StatusOK, planets)
}
