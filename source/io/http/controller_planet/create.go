package controller_planet

import (
	"aplicacao/source/domain/requests"
	"aplicacao/source/service"
	"aplicacao/source/utils/controller_utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreatePLanet godoc
// @Summary     Create planet
// @Description Create a new planet
// @Tags        Planet
// @Accept      json
// @Produce     json
// @Param       planet body     requests.PlanetRequest true "Add planet"
// @Success     200    {string} string                 "OK"
// @Failure     400    {object} exception.HttpException
// @Failure     401    {object} middleware.LoginError
// @Failure     403    {string} string "Forbidden"
// @Failure     404    {object} exception.HttpException
// @Security    ApiKeyAuth
// @Router      /planets [post]
func CreatePlanet(c *gin.Context) {
	planet := requests.PlanetRequest{}
	controller_utils.ReadBody(c, &planet)
	service.InsertPlanet(&planet)
	c.Status(http.StatusOK)
}
