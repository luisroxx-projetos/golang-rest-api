package controller_planet

import (
	"aplicacao/source/service"
	"aplicacao/source/utils/controller_utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

// DeletePLanet godoc
// @Summary     Delete planet
// @Description Pass an ID and delete a planet
// @Tags        Planet
// @Accept      json
// @Produce     json
// @Param       id  path     int    true "Planet ID"
// @Success     200 {string} string "OK"
// @Failure     400 {object} exception.HttpException
// @Failure     401 {object} middleware.LoginError
// @Failure     403 {string} string "Forbidden"
// @Failure     404 {object} exception.HttpException
// @Security    ApiKeyAuth
// @Router      /planets/{id} [delete]
func DeletePlanet(c *gin.Context) {
	id := controller_utils.ConvertToInt(c.Params.ByName("id"))
	service.DeletePlanet(id)
	c.Status(http.StatusOK)
}
