package controller_planet

import (
	"aplicacao/source/domain/requests"
	"aplicacao/source/service"
	"aplicacao/source/utils/controller_utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

// UpdatePlanet godoc
// @Summary     Update planet
// @Description Pass an ID and a requestobject to update planet
// @Tags        Planet
// @Accept      json
// @Produce     json
// @Param       id     path     int                    true "Planet ID"
// @Param       planet body     requests.PlanetRequest true "Add planet"
// @Success     200    {string} string                 "OK"
// @Failure     400    {object} exception.HttpException
// @Failure     401    {object} middleware.LoginError
// @Failure     403    {string} string "Forbidden"
// @Failure     404    {object} exception.HttpException
// @Security    ApiKeyAuth
// @Router      /planets/{id} [put]
func UpdatePlanet(c *gin.Context) {
	updatePlanetRequest := requests.PlanetRequest{}
	controller_utils.ReadBody(c, &updatePlanetRequest)

	id := controller_utils.ConvertToInt(c.Params.ByName("id"))
	service.UpdatePlanet(&updatePlanetRequest, id)
	c.Status(http.StatusOK)
}
