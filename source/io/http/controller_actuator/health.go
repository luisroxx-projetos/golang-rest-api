package controller_actuator

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// Health godoc
// @Summary     Auth admin
// @Description Discover application health
// @Tags        Actuator
// @Accept      json
// @Produce     json
// @Success     200 {object} string "{"status": "OK"}"
// @Router      /health [get]
func Health(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"status": "OK",
	})
}
