package controller_gorillaws

import (
	"aplicacao/source/io/websocket/gorillaws"
	"aplicacao/source/io/websocket/hubmanager"
	"net/http"

	"github.com/gin-gonic/gin"
)

func GorillaWebsocket(c *gin.Context) {
	roomname := c.Query("roomname")
	hub, err := hubmanager.RetrieveHub(roomname)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	gorillaws.GorillaHandler(hub, c.Writer, c.Request)
}
