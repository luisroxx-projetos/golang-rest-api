package controller_rabbitmq

import (
	"aplicacao/source/io/rabbitmq"
	"net/http"

	"github.com/gin-gonic/gin"
)

// SendMessage godoc
// @Summary     Send Message
// @Description Send a message to RabbitMQ
// @Tags        RabbitMQ
// @Accept      json
// @Produce     json
// @Success     200 {string} string "OK"
// @Failure     500 {string} string "error"
// @Router      /rabbitmq [get]
func SendMessage(c *gin.Context) {
	rabbitmq.SendMessage("Testando envio")
	c.Status(http.StatusOK)
}
