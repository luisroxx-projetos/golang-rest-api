package rabbitmq

import (
	"aplicacao/internal/rabbitmq"
	"aplicacao/source/configuration"
	"log"
)

var producer *rabbitmq.AmqpSession
var Consumer *rabbitmq.AmqpSession

func InitRabbitMQ() {
	producer = rabbitmq.NewQueue(configuration.AMPQ_SOURCE.Get(), configuration.AMPQ_QUEUE_NAME.Get())
	Consumer = rabbitmq.NewQueue(configuration.AMPQ_SOURCE.Get(), configuration.AMPQ_QUEUE_NAME.Get())

	startListeningToMessages(func(i string) {
		log.Printf("Received message: %s", i)
	})
}

func startListeningToMessages(callback func(i string)) {
	Consumer.Consume(callback)
}

func SendMessage(message string) {
	producer.Send(message)
}
