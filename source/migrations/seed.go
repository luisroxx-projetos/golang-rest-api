package migrations

import (
	"aplicacao/source/domain/entities"
	"aplicacao/source/domain/enumerations"
	"aplicacao/source/repository"
)

func InsertBaseUsers() {
	repository.CreateUser(&entities.User{
		Name:     "admin",
		Username: "admin",
		Password: "admin",
		Role:     enumerations.ADMIN,
	})
	repository.CreateUser(&entities.User{
		Name:     "supervisor",
		Username: "supervisor",
		Password: "supervisor",
		Role:     enumerations.SUPERVISOR,
	})
	repository.CreateUser(&entities.User{
		Name:     "normal",
		Username: "normal",
		Password: "normal",
		Role:     enumerations.NORMAL,
	})
}
