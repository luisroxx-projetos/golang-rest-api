package migrations

import (
	"aplicacao/source/domain/entities"
	"aplicacao/source/repository"
)

func AutoMigrate() {
	repository.DB.AutoMigrate(entities.RetrieveAll()...)
}

func DropAll() {
	repository.DB.DropTable(entities.RetrieveAll()...)
}
