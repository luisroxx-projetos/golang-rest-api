package repository

import (
	"aplicacao/source/domain/entities"
)

func FindUserByUsername(username string, tx ...*TransactionalOperation) (*entities.User, error) {
	user := &entities.User{}
	return user, withTransaction(tx).Where("username = ?", username).First(user).Error
}

// TransactionalOperations
func CreateUser(user *entities.User, tx ...*TransactionalOperation) error {
	return withTransaction(tx).Create(user).Error
}
