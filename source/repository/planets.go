package repository

import (
	"aplicacao/source/domain/entities"

	"github.com/jinzhu/gorm"
)

func FindPlanetsPaginated(limit int, page int, tx ...*TransactionalOperation) (*Pagination, error) {
	pagination := &Pagination{Limit: limit, Page: page}
	planets := []entities.Planet{}

	transaction := withTransaction(tx)
	err := transaction.Scopes(paginateScope(planets, pagination, transaction)).Find(&planets).Error

	pagination.Rows = planets

	return pagination, err
}

func ListPlanets(tx ...*TransactionalOperation) ([]entities.Planet, error) {
	planets := []entities.Planet{}
	return planets, withTransaction(tx).Find(&planets).Error
}

func FindPlanetById(id int, tx ...*TransactionalOperation) (*entities.Planet, error) {
	planet := &entities.Planet{}
	return planet, withTransaction(tx).Where("id = ?", id).First(planet).Error
}

func ExistsPlanetByName(name string, tx ...*TransactionalOperation) bool {
	dbResult := withTransaction(tx).Where("name = ?", name).Find(&entities.Planet{})
	return dbResult.RowsAffected > 0
}

// TransactionalOperations
func CreatePlanet(planet *entities.Planet, tx ...*TransactionalOperation) error {
	return withTransaction(tx).Create(planet).Error
}

func UpdatePlanet(planet *entities.Planet, tx ...*TransactionalOperation) error {
	return withTransaction(tx).Model(&entities.Planet{}).Updates(planet).Error
}

func DeletePlanet(id int, tx ...*TransactionalOperation) error {
	dbResult := withTransaction(tx).Delete(entities.Planet{Id: id})

	if dbResult.RowsAffected < 1 {
		return gorm.ErrRecordNotFound
	}

	return dbResult.Error
}
