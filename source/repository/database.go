package repository

import (
	"aplicacao/source/configuration"
	"aplicacao/source/domain/exception"
	"fmt"
	"log"
	"math"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var DB *gorm.DB

func InitDB() {
	var err error

	DB, err = gorm.Open(configuration.DATABASE_DRIVER.Get(), getConnectionString())

	if err != nil {
		log.Panic("An error ocurred during try to connect a database ", err)
	}
}

func getConnectionString() string {
	return fmt.Sprintf(
		"%s:%s@%s",
		configuration.DATABASE_USERNAME.Get(),
		configuration.DATABASE_PASSWORD.Get(),
		configuration.DATABASE_SOURCE.Get(),
	)
}

// Transaction
type TransactionalOperation struct {
	transaction *gorm.DB
}

func UsingTransactional(fn func(*TransactionalOperation) error) {
	err := DB.Transaction(func(tx *gorm.DB) error {
		return fn(&TransactionalOperation{transaction: tx})
	})
	if err != nil {
		except, ok := err.(*exception.HttpException)
		if ok {
			panic(except)
		}

		panic(exception.InternalServerException(err.Error()))
	}
}

func withTransaction(tx []*TransactionalOperation) *gorm.DB {
	for idx, t := range tx {
		if t != nil {
			return tx[idx].transaction
		}
	}

	return DB
}

// Pagination
type Pagination struct {
	Limit      int         `json:"limit"`
	Page       int         `json:"page"`
	Sort       string      `json:"sort"`
	TotalRows  int64       `json:"total_rows"`
	TotalPages int         `json:"total_pages"`
	Rows       interface{} `json:"rows"`
}

func (p *Pagination) GetOffset() int {
	return (p.GetPage() - 1) * p.GetLimit()
}

func (p *Pagination) GetLimit() int {
	if p.Limit <= 0 {
		p.Limit = 10
	}

	return p.Limit
}

func (p *Pagination) GetPage() int {
	if p.Page <= 0 {
		p.Page = 1
	}
	return p.Page
}

func (p *Pagination) GetSort() string {
	if p.Sort == "" {
		p.Sort = "Id asc"
	}
	return p.Sort
}

func paginateScope(model interface{}, pagination *Pagination, db *gorm.DB) func(db *gorm.DB) *gorm.DB {
	totalRows := int64(0)
	db.Model(model).Count(&totalRows)

	pagination.TotalRows = totalRows
	pagination.TotalPages = int(math.Ceil(float64(totalRows) / float64(pagination.GetLimit())))

	return func(db *gorm.DB) *gorm.DB {
		return db.Offset(pagination.GetOffset()).Limit(pagination.GetLimit()).Order(pagination.GetSort())
	}
}
