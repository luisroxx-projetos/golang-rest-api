//go:build prod
// +build prod

package configuration

import "github.com/gin-gonic/gin"

func init() {
	//Environment
	APPLICATION_ENVIRONMENT.Set("prod")

	//Database
	DATABASE_DRIVER.UpdateValueFromEnv("DATABASE_DRIVER")
	DATABASE_NAME.UpdateValueFromEnv("DATABASE_NAME")
	DATABASE_USERNAME.UpdateValueFromEnv("DATABASE_USERNAME")
	DATABASE_PASSWORD.UpdateValueFromEnv("DATABASE_PASSWORD")
	DATABASE_SOURCE.UpdateValueFromEnv("DATABASE_SOURCE")

	//AMPQ
	AMPQ_USERNAME.UpdateValueFromEnv("AMPQ_USERNAME")
	AMPQ_PASSWORD.UpdateValueFromEnv("AMPQ_PASSWORD")
	AMPQ_SOURCE.UpdateValueFromEnv("AMPQ_SOURCE")
	AMPQ_QUEUE_NAME.UpdateValueFromEnv("AMPQ_QUEUE_NAME")
	AMPQ_QUEUE_DURABLE.UpdateValueFromEnv("AMPQ_QUEUE_DURABLE")
	AMPQ_QUEUE_DELETED_WHEN_UNUSED.UpdateValueFromEnv("AMPQ_QUEUE_DELETED_WHEN_UNUSED")
	AMPQ_QUEUE_EXCLUSIVE.UpdateValueFromEnv("AMPQ_QUEUE_EXCLUSIVE")
	AMPQ_QUEUE_NO_WAIT.UpdateValueFromEnv("AMPQ_QUEUE_NO_WAIT")

	//HubManager
	HUB_IDLE_TIMEOUT.UpdateValueFromEnv("HUB_IDLE_TIMEOUT")

	//Jwt
	JWT_SECRET_KEY.UpdateValueFromEnv("JWT_SECRET_KEY")
	JWT_IDENTITY_KEY.UpdateValueFromEnv("JWT_IDENTITY_KEY")
	JWT_REALM.UpdateValueFromEnv("JWT_REALM")
	JWT_TIMEOUT.UpdateValueFromEnv("JWT_TIMEOUT")
	JWT_MAX_REFRESH.UpdateValueFromEnv("JWT_MAX_REFRESH")

	gin.SetMode(gin.ReleaseMode)
}
