//go:build test
// +build test

package configuration

func init() {
	//Environment
	APPLICATION_ENVIRONMENT.Set("test")

	//Database
	DATABASE_NAME.Set("gorest-test")
	DATABASE_SOURCE.Set("tcp(host.docker.internal:3306)/gorest-test?charset=utf8&parseTime=True&loc=Local")
}
