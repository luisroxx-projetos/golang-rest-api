//go:build stg
// +build stg

package configuration

func init() {
	//Environment
	APPLICATION_ENVIRONMENT.Set("stg")

	//Database
	DATABASE_NAME.Set("gorest-stg")
	DATABASE_SOURCE.Set("tcp(host.docker.internal:3306)/gorest-stg?charset=utf8&parseTime=True&loc=Local")
}
