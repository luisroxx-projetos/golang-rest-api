package configuration

import (
	"aplicacao/internal/config"
	"time"
)

var (
	//Environment
	APPLICATION_ENVIRONMENT = config.MakeConfig("dev")

	//Database
	DATABASE_DRIVER   = config.MakeConfig("mysql")
	DATABASE_NAME     = config.MakeConfig("gorest-dev")
	DATABASE_USERNAME = config.MakeConfig("root")
	DATABASE_PASSWORD = config.MakeConfig("root")
	DATABASE_SOURCE   = config.MakeConfig("tcp(host.docker.internal:3306)/gorest-dev?charset=utf8&parseTime=True&loc=Local")

	//AMPQ
	AMPQ_USERNAME                  = config.MakeConfig("root")
	AMPQ_PASSWORD                  = config.MakeConfig("root")
	AMPQ_SOURCE                    = config.MakeConfig("amqp://root:root@host.docker.internal:5672/")
	AMPQ_QUEUE_NAME                = config.MakeConfig("test")
	AMPQ_QUEUE_DURABLE             = config.MakeConfig(false)
	AMPQ_QUEUE_DELETED_WHEN_UNUSED = config.MakeConfig(false)
	AMPQ_QUEUE_EXCLUSIVE           = config.MakeConfig(false)
	AMPQ_QUEUE_NO_WAIT             = config.MakeConfig(false)

	//HubManager
	HUB_IDLE_TIMEOUT = config.MakeConfig(time.Hour * 24)

	//Jwt
	JWT_SECRET_KEY   = config.MakeConfig([]byte("my-jwt-secret-key"))
	JWT_IDENTITY_KEY = config.MakeConfig("name")
	JWT_REALM        = config.MakeConfig("realm")
	JWT_TIMEOUT      = config.MakeConfig(time.Hour)
	JWT_MAX_REFRESH  = config.MakeConfig(time.Hour)
)
